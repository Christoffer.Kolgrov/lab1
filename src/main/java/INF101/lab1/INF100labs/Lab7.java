package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        if (row >= 0 && row < grid.size()) {
            grid.remove(row);
        }
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        if (grid == null || grid.size() == 0) {
            return false;
        }
    
        int n = grid.size();
        int[] rowSums = new int[n];
        int[] colSums = new int[n];
    
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < grid.get(i).size(); j++) {
                rowSums[i] += grid.get(i).get(j);
                colSums[j] += grid.get(i).get(j);
            }
        }
    
        for (int i = 1; i < n; i++) {
            if (rowSums[i] != rowSums[0] || colSums[i] != colSums[0]) {
                return false;
            }
        }
    
        return true;    }

}