package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        list.removeIf(n -> n == 3);
        return list;
    }

    public static List<Integer> uniqueValues(ArrayList<Integer> list) {
        Set<Integer> seen = new HashSet<>();
        list.removeIf(n -> !seen.add(n));
        return list;    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < a.size(); i++) {
            a.set(i, a.get(i) + b.get(i));
        }
    }

}