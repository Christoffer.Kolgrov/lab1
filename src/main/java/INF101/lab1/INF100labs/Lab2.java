package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int len1 = word1.length();
        int len2 = word2.length();
        int len3 = word3.length();

        int maxLength = Math.max(len1, Math.max(len2, len3));

        if (len1 == maxLength) {
            System.out.println(word1);
        }
        if (len2 == maxLength) {
            System.out.println(word2);
        }
        if (len3 == maxLength) {
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 4 == 0) {
            if (year % 400 == 0) {
                return true;
            } 
            if (year % 100 == 0) {
                return false;
            }

            return true;
        }
        return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        return num > 0 && num % 2 == 0;
    }

}
